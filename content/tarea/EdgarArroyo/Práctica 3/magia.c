#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char zip[] = {0x50, 0x4B, 0x00};
unsigned char png[] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00};
unsigned char elf[] = {0x7F, 0x45, 0x4C, 0x46, 0x00};
unsigned char gif[] = {0x47, 0x49, 0x46, 0x38, 0x00};
unsigned char pdf[] = {0x25, 0x50, 0x44, 0x46, 0x2d, 0x00};
unsigned char mp3[] = {0xFF, 0xFB, 0x00};
unsigned char mp3ID3[] = {0x49, 0x44, 0x33, 0x00};
unsigned char exe[] = {0x4D, 0x5A, 0x00};

void imprime_magic(const char *archivo)
{
    FILE *stream = fopen(archivo, "rb");
    if (!stream)
        printf("Error al abrir %s\n", archivo);
    else
    {
        unsigned char buffer[9], *posible, *formato;
        int num_bytes_leidos = fread(buffer, 1, 8, stream);

        if (num_bytes_leidos == 8)
        {
            switch (buffer[0])
            {
            case 0x47:
                posible = gif;
                formato = "GIF";
                break;

            case 0x25:
                posible = pdf;
                formato = "PDF";
                break;

            case 0xff:
                posible = mp3;
                formato = "MP3";
                break;

            case 0x49:
                posible = mp3ID3;
                formato = "MP3 con etiquetas";
                break;

            case 0x4D:
                posible = exe;
                formato = "EXE";
                break;

            case 0x50:
                posible = zip;
                formato = "ZIP";
                break;

            case 0x89:
                posible = png;
                formato = "PNG";
                break;

            case 0x7F:
                posible = elf;
                formato = "ELF";
                break;

            default:
                posible = "";
                formato = "DESCONOCIDO";
                break;
            }
            buffer[strlen(posible)] = 0x00;
            if (strcmp(buffer, posible))
                formato = "DESCONOCIDO";
        }
        else
            formato = "DESCONOCIDO";

        printf("%s: archivo %s\n", archivo, formato);
        fclose(stream);
    }
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        printf("Modo de ejecución:\n%s archivo1 [archivo2...]\n", argv[0]);
        exit(1);
    }

    char const *archivo;
    for (int i = 1; i < argc; ++i)
    {
        archivo = argv[i];
        imprime_magic(archivo);
    }
    return 0;
}