#include <openssl/evp.h>
#include <openssl/err.h>
#include <stdio.h>

void error(const char *mensaje, const char *otro)
{
    printf("%s %s\n", mensaje, otro);
    exit(1);
}

void imprime_md5(const char *archivo, const char *func_hash)
{
    FILE *stream = fopen(archivo, "rb");
    if (!stream)
        error("Error al abrir", archivo);
    unsigned char buffer[100];
    unsigned char hash[64];
    unsigned int tamano_hash;
    const EVP_MD *md = EVP_get_digestbyname(func_hash);
    if (NULL != md)
    {
        EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
        EVP_MD_CTX_init(mdctx);
        EVP_DigestInit_ex(mdctx, md, NULL);
        int num_bytes_leidos;
        while (num_bytes_leidos = fread(buffer, 1, 100, stream))
        {
            EVP_DigestUpdate(mdctx, buffer, num_bytes_leidos);
        }
        EVP_DigestFinal_ex(mdctx, hash, &tamano_hash);
        EVP_MD_CTX_free(mdctx);
    }
    else
    {
        error("Error en el nombre de la función hash", func_hash);
    }
    fclose(stream);
    for (int i = 0; i < tamano_hash; ++i)
    {
        printf("%02x", hash[i]);
    }
    printf("  %s\n", archivo);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        error("Modo de ejecución:\nhash func_hash archivo1 [archivo2...]", "");
    }
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    unsigned char *opcion_hash = argv[1];
    unsigned char *archivo;
    for (int i = 2; i < argc; ++i)
    {
        archivo = argv[i];
        imprime_md5(archivo, opcion_hash);
    }
    EVP_cleanup();
    CRYPTO_cleanup_all_ex_data();
    ERR_free_strings();
    return 0;
}