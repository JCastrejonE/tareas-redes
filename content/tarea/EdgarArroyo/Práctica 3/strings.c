#include <stdio.h>
#include <stdlib.h>

void error(const char *mensaje, const char *otro)
{
    printf("%s %s\n", mensaje, otro);
    exit(1);
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
        error("Modo de ejecución:\nstrings archivo [long_min]", "");
    FILE *stream = fopen(argv[1], "rb");
    if (!stream)
        error("Error al abrir", argv[1]);
    unsigned char byte;
    /* Longitud minima de cadenas a imprimir. Por default 4 */
    int long_min = argc > 2 ? atoi(argv[2]) : 4;
    unsigned char *buffer = malloc(long_min);
    int c = 0, print_bufer = 1, primera_linea = 1;
    while (fread(&byte, 1, 1, stream))
    {
        /* Para version que no usa longitud minima
        if (byte >= 32 && byte <= 126)
        {
            if (nueva_linea) {
                printf("\n");
                nueva_linea = 0;
            }
            printf("%c", byte);

        }
        else
            nueva_linea = 1;
        */
        if (byte >= 32 && byte <= 126)
        {
            if (c < long_min - 1)
            {
                buffer[c] = byte;
                c++;
            }
            else
            {
                if (primera_linea && print_bufer)
                {
                    printf("%s", buffer);
                    print_bufer = primera_linea = 0;
                }
                else if (print_bufer)
                {
                    printf("\n%s", buffer);
                    print_bufer = 0;
                }
                printf("%c", byte);
            }
        }
        else
        {
            c = 0;
            print_bufer = 1;
        }
    }
    printf("\n");
    fclose(stream);
    return 0;
}