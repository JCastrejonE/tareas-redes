#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h> // Para usar cpsas como strcm strcat, strlen
#include <arpa/inet.h> // Para usar inet_addr
#include <unistd.h>

int main(int argc, char *argv[]){

  // Variables
  int sockfd, newsockfd, port;
  socklen_t clilen;
  char input[512] = {0};
  char output[512] = {0};
  char command[20] = {0};
  struct sockaddr_in serv_addr, cli_addr;
  char str[INET_ADDRSTRLEN];

  // Lo usamos con popen
  FILE *fp;

  // Validamos que nos pase el puerto a abrir
  if (argc < 2) {
    fprintf(stderr, "Falta el puerto\n");
    return 1;
   }

  // Creamos el socket
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Error al crear el socket\n");
    return 1;
  }
  printf("Socket creado correctamente\n");

  // Limpiamos la memoria donde está serv_addr
  bzero((char *) &serv_addr, sizeof(serv_addr));

  // Convertimos a int el puerto
  port = atoi(argv[1]);

  // Cocinamos la estructura sockaddr_in
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);

  // Ligamos
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    fprintf(stderr, "Error al enlazar\n");
    return 1;
  }
  printf("Ligado correctamente\n");

  // Escuchamos
  if (listen(sockfd, 3) < 0){
    fprintf(stderr, "Error al escuchar\n");
    return 1;
  }
  printf("Escuchando, esperando conexiones entrantes\n");

  // Aceptamos conexiones entrantes del cliente
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);

  printf("server: Conexión aceptada desde %s:%d\n", str, port);

  for(;;){

    // Limpiamos la memoria donde está input
    bzero(input,sizeof(input));
    // Limpiamos la memoria donde está output
		bzero(output,sizeof(output));
    // Limpiamos la memoria donde está command
		bzero(command,sizeof(command));

    // Leemos y guardamos en input
		read(newsockfd, input, sizeof(input));

    // Comparamos input con la cadena EOF para ver si debemos cerrar la conexión

		if(strcmp(input, "EOF") == 0)
      break;
    // Sólo para tener logs de qué estamos ejecutando del lado del servidor
		printf("Comando a ejecutar: %s\n", input);

    // Ejecutamos el comando
    // Nota: Uso popen porque me permite recuperar el resultado de la
    // ejecución del comando
		fp = popen(input,"r");

    // Aquí empieza un intento fallido de poder ejecutar comandos con
    // respuesta nula.
    // e.g. echo 1 > hola

    // Obtengo la primera línea
    // char *fl = fgets(command, sizeof(command), fp);

    // Debugging
    // printf("***%s***\n", fl);

    // Checo si es nula
    // if (fl == NULL) // Esta parte era debug
    //   pass; // No hago nada
    // else{ // Si trae más cosas la respuesta
      // Pongo la primera línea que ya tengo en mi output
      // strcat(output, fl);
      // La sigo concatenando para regresarla completa
    while(fgets(command, sizeof(command), fp) != NULL)
      strcat(output,command);
    // }

    // Si sólo dejo el while dentro del else (sin el primer if NULL)
    // igual sirve, pero intenté poder ejecutar comando con respuesta nula
    // Actualmente no sirven cosas como:
    // echo 1 > hola
    // rm hola

    // Cerramos fp
		pclose(fp);

    // Escribimos
		write(newsockfd,output,strlen(output));

    // Debugging
    // printf("---------%s---------\n", output);
	}

	close(newsockfd);
	printf("Se ha cerrado la conexión\n");

  return 0;
}
