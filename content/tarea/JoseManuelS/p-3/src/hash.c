/*
 * Redes de Computadoras 2019-2
 * Martinez Sanchez Jose Manuel
 * hash.c
 * gcc -g -Wall -o hash hash.c -lssl -lcrypto
 */
#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <stdio.h>
#include <openssl/md5.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <stdlib.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#define STR_VALUE(val) #val
#define STR(name) STR_VALUE(name)

#define PATH_LEN 256
#define MD5_LEN 32



//Funcion auxiliar
void get_hash_string (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[65]){
    int i = 0;

    for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }

    outputBuffer[64] = 0;
}


//sha256sum-------------------------------------------
int sha256_file(char *path, char outputBuffer[65]){
    FILE *file = fopen(path, "rb");
    if(!file) return -534;

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    const int bufSize = 32768;
    unsigned char *buffer = malloc(bufSize);
    int bytesRead = 0;
    if(!buffer) return ENOMEM;
    while((bytesRead = fread(buffer, 1, bufSize, file))){
      SHA256_Update(&sha256, buffer, bytesRead);
    }
    SHA256_Final(hash, &sha256);

    get_hash_string(hash, outputBuffer);
    fclose(file);
    free(buffer);
    return 0;
}




//md5sum ----------------------------------------

int CalcFileMD5(char *file_name, char *md5_sum){
    #define MD5SUM_CMD_FMT "md5sum %." STR(PATH_LEN) "s 2>/dev/null"
    char cmd[PATH_LEN + sizeof (MD5SUM_CMD_FMT)];
    sprintf(cmd, MD5SUM_CMD_FMT, file_name);
    #undef MD5SUM_CMD_FMT

    FILE *p = popen(cmd, "r");
    if (p == NULL) return 0;

    int i, ch;
    for (i = 0; i < MD5_LEN && isxdigit(ch = fgetc(p)); i++) {
        *md5_sum++ = ch;
    }

    *md5_sum = '\0';
    pclose(p);
    return i == MD5_LEN;
}




// sha1sum ---------------------------------------

int sha1(char *file) {
  SHA_CTX ctx;
  size_t len;
  unsigned char buffer[BUFSIZ];
  FILE *f;
  f = fopen(file, "r");
  if (!f) {
      fprintf(stderr, "Archivo invalido %s\n", file);
      return 1;
  }
  SHA1_Init(&ctx);
  do {
      len = fread(buffer, 1, BUFSIZ, f);
      SHA1_Update(&ctx, buffer, len);
  } while (len == BUFSIZ);

  SHA1_Final(buffer, &ctx);

  fclose(f);

  for (len = 0; len < SHA_DIGEST_LENGTH; ++len)
        printf("%02x", buffer[len]);
  putchar('\n');
  return 0;
}


int main(int argc, char **argv){
    if (argc < 3) {
      printf("NO ingreso todos los parametros!\n");
      exit(-1);
    }

    for (int i = 2; i < argc; ++i){
      if (0 == strcmp(argv[1], "md5")) {
        char md5[MD5_LEN + 1];
        if (!CalcFileMD5(argv[i], md5)) {
            puts("Imposible calcular md5sum!");
        } else {
            printf("md5sum: %s\n", md5);
        }
      } else if (0 == strcmp(argv[1], "sha1")) {
        sha1(argv[i]);
      } else if (0 == strcmp(argv[1], "sha256")) {
        char calc_hash[65];
        sha256_file(argv[i],calc_hash);
        printf("%s\n",calc_hash);
      } else {
        printf("%s\n","No ingreso ninguna opcion valida.");
      }
    }


    return 0;
}

/*
 * hash.c
*/
