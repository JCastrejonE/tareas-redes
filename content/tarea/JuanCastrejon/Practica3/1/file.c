#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    printf("Uso: %s <archivo1> [<archivo2> ...]\n", argv[0]);
    return 1;
  }

  FILE *fp;
  char buffer[10];
  int i = 0;
  while(++i < argc) {
    fp = fopen(argv[i], "rb");
    if (fp != NULL)
    {
      fread(buffer, sizeof(buffer), 1, fp);
      if(strncmp(buffer, "\x47\x49\x46\x38\x37\x61", 6) == 0)
        printf("%s: Archivo GIF\n", argv[i]);
      else if(strncmp(buffer, "\x47\x49\x46\x38\x39\x61", 6) == 0)
        printf("%s: Archivo GIF\n", argv[i]);
      else if(strncmp(buffer, "\x7F\x45\x4C\x46", 4) == 0)
        printf("%s: Archivo ELF\n", argv[i]);
      else if(strncmp(buffer, "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8) == 0)
        printf("%s: Archivo PNG\n", argv[i]);
      else if(strncmp(buffer, "\x50\x4b\x03\x04", 4) == 0)
        printf("%s: Archivo ZIP\n", argv[i]);
      else if(strncmp(buffer, "\x25\x50\x44\x46\x2d", 5) == 0)
        printf("%s: Archivo PDF\n", argv[i]);
      else if(strncmp(buffer, "\xFF\xFB", 2) == 0)
        printf("%s: Archivo MP3\n", argv[i]);
      else if(strncmp(buffer, "\x49\x44\x33", 3) == 0)
        printf("%s: Archivo MP3\n", argv[i]);
      else if(strncmp(buffer, "\x4D\x5A", 2) == 0)
        printf("%s: Archivo EXE\n", argv[i]);
      else {
        printf("%s: Archivo no identificado\n", argv[i]);
      }
      fclose(fp);
    }
    else
    {
      perror(argv[i]);
    }
  }
}
