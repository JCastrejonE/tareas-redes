# Práctica 1: Programación en C

## Ejercicios

### 1. Lista enlazada

Usando el siguiente programa escrito en 1/main.c:
```c
int main()
{
  struct lista lista = {.cabeza=0, .longitud=0};
  for (int c = 'a'; c <= 'k'; c++)
  {
    char *e = (char *)malloc(sizeof(e));
    *e = c;
    agrega_elemento(&lista, e);
  }
  printf("Longitud de lista: %d\nElementos: ", lista.longitud);
  aplica_funcion(&lista, imprime);
  aplica_funcion(&lista, mayuscula);
  printf("\nMasyúsculas: ");
  aplica_funcion(&lista, imprime);
  int n = lista.longitud;
  for (int i = 0; i < n; i++)
  {
    printf("\nElemento a ser eliminado: %c, ", *(char *)obten_elemento(&lista, 0));
    char *e = (char *)elimina_elemento(&lista, 0);
    printf("elemento eliminado: %c, longitud de lista: %d", *e, lista.longitud);
    free(e);
  }
  puts("");
}
```
se produce la siguiente salida:
![Ejemplo1](imgs/1.png)

### 2. Manejo de archivos

Ejemplo de ejecución:
![Ejemplo2](imgs/2.png)

### e2. /etc/passwd
Ejemplo de ejecución:
![Ejemplo2](imgs/3.png)
