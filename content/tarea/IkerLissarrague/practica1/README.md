# Práctica 1

## Compilación
```sh
$ make
```
## Imagenes de Ejecución

### ejemplo

Ejecución de ejemplo.
![N|Solid](img/ejemplo.png)

### archivo

Agregando archivos/directorios.
![N|Solid](img/archivos_insercion.png)

Leyendo archivos/directorios.
![N|Solid](img/archivos_leer.png)
![N|Solid](img/archivos_leer_2.png)

Eliminando/Imprimiendo archivos/directorios.
![N|Solid](img/eliminar_imprimir.png)
