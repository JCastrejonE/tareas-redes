#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE 10

int main(int argc, char *argv[]){
	FILE* file;
	unsigned char buffer[BUFF_SIZE];
	for (int i = 1; i < argc; ++i){
		if (file = fopen(argv[i], "r")){
		    fread(buffer,1,BUFF_SIZE,file);
		    switch(buffer[0]){
		    	//47 49 46 38 37 61 GIF87a, 47 49 46 38 39 61 GIF89a
		    	case 0x47:
		    		if (buffer[1]==0x49&&buffer[2]==0x46&&buffer[3]==0x38&&buffer[5]==0x61)
		    			printf("El archivo %s es un archivo de tipo GIF\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
	    		//7F 45 4C 46 .ELF
		    	case 0x7F:
		    		if(buffer[1]==0x45&&buffer[2]==0x43&&buffer[3]==0x4C&&buffer[4]==0x46)
			    		printf("El archivo %s es un archivo de tipo ELF\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	//89 50 4E 47 0D 0A 1A 0A .PNG
		    	case 0x89:
		    		if(buffer[1]==0x50&&buffer[2]==0x4E&&buffer[3]==0x47&&buffer[4]==0x0D&&buffer[5]==0x0A&&buffer[6]==0x1A&&buffer[7]==0x0A)
		    			printf("El archivo %s es un archivo de tipo PNG\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	//50 4B 03 04 .ZIP, 50 4B 05 06 EMPTY .ZIP, 50 4B 07 08 SPANNED .ZIP
		    	case 0x50:
		    		if(buffer[2]==0x4B)
		    			printf("El archivo %s es un archivo de tipo ZIP\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	//25 50 44 46 2D .PDF
		    	case 0x25:
		    		if(buffer[1]==0x50&&buffer[2]==0x44&&buffer[3]==0x46&&buffer[4]==0x2D)
		    			printf("El archivo %s es un archivo de tipo PDF\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	//FF FB .MP3
		    	case 0xFF:
		    		if(buffer[1]==0xFB)
		    			printf("El archivo %s es un archivo de tipo MP3\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	//4D 5A .EXE
		    	case 0x4D:
		    		if(buffer[1]==0x51)
		    			printf("El archivo %s es un archivo de tipo EXE\n",argv[i]);
		    		else
		    			printf("No se reconoció el formato del archivo %s\n",argv[i]);
		    		break;
		    	default:
		    		printf("El archivo %s no tiene un formato reconocido\n",argv[i]);
		    		break;
		    }
		}	
		else
			fprintf(stderr,"Error leyendo archivo %d: %s\n",i,argv[i]);
	}	
}

