# Práctica 2

## Compilación
```sh
$ make
```
## Imagenes de Ejecución

Compilación
![N|Solid](img/compilación.png)

### servidor

Inicio de servidor.
![N|Solid](img/servidor_start.png)

Conexión con cliente aceptada.
![N|Solid](img/conexion_aceptada.png)

EOF
![N|Solid](img/EOF_servidor.png)

### cliente

Inicio de cliente (el servidor está arriba).
![N|Solid](img/cliente_start.png)

Ejecución de un comando desde el cliente.
![N|Solid](img/comando_cliente.png)

EOF.
![N|Solid](img/EOF_cliente.png)
