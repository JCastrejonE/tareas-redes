#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void error(const char * msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[]){
	int client_fd,port,addr,pton_ret,n;
	char recvBuff[1025];
	struct sockaddr_in serv_addr;
	char cmd[101];
	if(argc<3)
		error("ERROR: Se debe de pasar como argumento:\n1)La direccion IPv4 del servidor\n2) El puerto al que se quiere conectar");
	client_fd=socket(AF_INET, SOCK_STREAM, 0);
	if(client_fd<0)
		error("ERROR: No se pudo abrir Socket");

	memset(recvBuff, '0',sizeof(recvBuff));
	port=atoi(argv[2]);
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_port=htons(port);

	pton_ret=inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);
	if(pton_ret==0)
		error("ERROR: IP invalida");
	if(pton_ret==-1)
		error("ERROR: Error convirtiendo IP");

	if(connect(client_fd, (struct sockaddr*)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR: Fallo en la conexión con el servidor");

	printf("Para terminar la conexión escribe 'EOF'\n");

	while(strcmp("EOF",cmd)!=0){
		bzero(cmd,sizeof(cmd));
		bzero(recvBuff,sizeof(recvBuff));
		//fgets(,100,cmd);
		fgets(cmd, 100, stdin);
		if( cmd[strlen(cmd)-1] == '\n')
    		cmd[strlen(cmd)-1] = 0;
		write(client_fd, cmd, strlen(cmd));
		//printf("Just sent message to server\n");
		read(client_fd, recvBuff, sizeof(recvBuff)-1);
		//printf("Answer from server:\n%s",recvBuff);
		printf("%s",recvBuff);
	}
	printf("Se ha terminado la conexión\n");
    return 0;

}