#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

void imprime(void * elemento) {
	char * e = (char*)elemento;
	printf("%c ", *e);
}

void mayuscula(void * elemento) {
	char * e = (char*)elemento;
	*e += 'A' - 'a';
}

// int main() {
// 	struct lista lista = {cabeza:0, longitud:0};
// 	for(int c = 'a'; c <= 'k'; c++) {
// 		char * e = (char*)malloc(sizeof(e));
// 		*e = c;
// 		agrega_elemento(&lista, e);
// 	}
// 	printf("Longitud de lista: %d\n", lista.longitud);
// 	struct nodo* n=lista.cabeza;
// 	for (int i = 0; i < lista.longitud; ++i) {
// 		char* e=(char*)n->elemento;
// 		printf("%c\n",*e);
// 		n=n->siguiente;
// 	}
// 	printf("And for my next trick:\n");
// 	for (int j = 0; j < lista.longitud; ++j) {
// 		char* e2=(char*)obten_elemento(&lista,j);
// 		printf("i:%d->%c\n",j,*e2);
// 	}
// 	for(int k=5;){

// 	}
// }

int main() {
	//struct lista lista = {cabeza:0, longitud:0}; <-- causes warning, instead do:
	struct lista lista = {.cabeza=0, .longitud=0};
	for(int c = 'a'; c <= 'k'; c++) {
		char * e = (char*)malloc(sizeof(e));
		*e = c;
		agrega_elemento(&lista, e);
	}
	printf("Longitud de lista: %d\nElementos: ", lista.longitud);
	aplica_funcion(&lista, imprime);
	aplica_funcion(&lista, mayuscula);
	printf("\nMasyúsculas: ");
	aplica_funcion(&lista, imprime);
	int n = lista.longitud;
	for(int i = 0; i < n; i++) {
		printf("\nElemento a ser eliminado: %c, ", *(char*)obten_elemento(&lista, 0));
		char * e = (char *)elimina_elemento(&lista, 0);
		printf("elemento eliminado: %c, longitud de lista: %d", *e, lista.longitud);
		free(e);
	}
	puts("");
}

