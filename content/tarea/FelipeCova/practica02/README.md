## Cova Pacheco Felipe de Jesús
##### Compilar:
make
##### Eliminar ejecutables:
make clean
##### Ejecutar:
1. Servidor: ./servidor < puerto >
2. Cliente: ./cliente < host > < puerto >

##### Compilar
![Imagen 1](screenshots/im1.png)
### Ejecución
![Imagen 2](screenshots/im2.png)
![Imagen 3](screenshots/im3.png)
![Imagen 4](screenshots/im4.png)
![Imagen 5](screenshots/im5.png)
