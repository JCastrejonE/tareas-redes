#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>
/*el codigo de conexion, asi como la funcion trim fueron obtenidas de tutoriales de internet*/
/*https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_C/Sockets*/
/*http://www.martinbroadhurst.com/trim-a-string-in-c.html*/

char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}

char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}



int main(int argc, char **argv){
  if(argc<2)
  {
    printf("%s <puerto>\n",argv[0]);
    return 1;
  }
  int conexion_servidor, conexion_cliente, puerto; 
  socklen_t longc; 
  struct sockaddr_in servidor, cliente;
  char param[100]; 
  puerto = atoi(argv[1]);
  conexion_servidor = socket(AF_INET, SOCK_STREAM, 0);
  bzero((char *)&servidor, sizeof(servidor)); 
  servidor.sin_family = AF_INET; 
  servidor.sin_port = htons(puerto);
  servidor.sin_addr.s_addr = INADDR_ANY;
  if(bind(conexion_servidor, (struct sockaddr *)&servidor, sizeof(servidor)) < 0)
  {
    printf("Error al asociar el puerto a la conexion\n");
    close(conexion_servidor);
    return 1;
  }

  listen(conexion_servidor, 3);
  printf("A la escucha en el puerto %d\n", ntohs(servidor.sin_port));
  longc = sizeof(cliente);
  conexion_cliente = accept(conexion_servidor, (struct sockaddr *)&cliente, &longc);
  if(conexion_cliente<0){
    printf("Error al aceptar trafico\n");
    close(conexion_servidor);
    return 1;
  }
  printf("Conectando con %s:%d\n", inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));



  int bandera = 1;
  while(bandera){
    if(recv(conexion_cliente, param, 100, 0) < 0)
      {
	printf("Error al recibir los datos\n");
	close(conexion_servidor);
	return 1;
      }
    else
      {
	if(strcmp(param,"salir") == 10){
	  printf("saliendo\n");
	  send(conexion_cliente, "Señal de salida\n", 20, 0);
	  bandera = 0;
	} else {
	  system(param);
	  FILE * arch;
	  char salidatxt[200];
	  char linea[200];
	  char * contparam = (char*)malloc(sizeof(contparam));
	  arch = fopen("salida.txt" , "r");
	  while(fgets(linea, 200, arch)){
	    strcpy(salidatxt,contparam);
	    contparam = strcat(salidatxt,trim(linea,"\t\v\f\r  "));
	  }
	  fclose(arch);
	  strcpy(salidatxt,contparam);
	  contparam = strcat(salidatxt,"\n");
	  system("rm salida.txt");
	  send(conexion_cliente, contparam, 200, 1);
	}
      }
  }

  close(conexion_servidor);
  return 0;
}
