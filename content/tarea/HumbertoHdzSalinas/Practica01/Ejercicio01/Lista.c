#include <stdio.h>
#include <stdlib.h>
#include<stdbool.h>

struct nodo
{
  void * elemento;
  struct nodo * siguiente;
};


struct Node 
{ 
    void * data; 
    struct Node * next; 
}; 

struct Lista
{
  struct nodo * siguiente;
  int longitud;
};





void obten(struct nodo *head)
{
  struct nodo *tmp = head;
  printf("%c ", tmp->elemento);
  tmp = tmp->siguiente;
}




void pusher(struct Node** head_ref, void *new_key) 
{ 
    /* allocate node */
    struct Node* new_node = 
            (struct Node*) malloc(sizeof(struct Node)); 
  
    /* put in the key  */
    new_node->data = new_key; 
  
    /* link the old list off the new node */
    new_node->next = (*head_ref); 
  
    /* move the head to point to the new node */
    (*head_ref)    = new_node; 
} 



void deleteNode(struct Node *node_ptr)    
{ 
   struct Node *temp = node_ptr->next; 
   node_ptr->data    = temp->data; 
   node_ptr->next    = temp->next; 
   free(temp); 
} 



void printList(struct Node *head) 
{ 
   struct Node *temp = head; 
   while(temp != NULL) 
   { 
      printf("%c  ", temp->data); 
      temp = temp->next; 
   } 
} 



int main()
{

  struct Node* head = NULL;
  printf("\n");
  char j = 'k';
  pusher(&head, j);
  pusher(&head, 'j');
  pusher(&head, 'i');
  pusher(&head, 'h');
  pusher(&head, 'g');
  pusher(&head, 'f');
  pusher(&head, 'e');
  pusher(&head, 'd');
  pusher(&head, 'c');
  pusher(&head, 'b');
  pusher(&head, 'a');

  printf("Elementos: ");
  printList(head);
  printf("\n");
  //obten(head);
  printf("\n");
  //printList(head);
  printf("\nEl elemento a ser eliminado:  ");
  obten(head);
  printf(", Elemento eliminado: ");
  obten(head);
  printf("\n");
  deleteNode(head);
  printf("\nEl elemento a ser eliminado:  ");
  obten(head);
  printf(", Elemento eliminado: ");
  obten(head);
  deleteNode(head);
  printf("\n");
  //printList(head);
  //search(head, 10)? printf("\nYes\n") : printf("No"); 


  //  printf("\n A partir de aqui empieza la parte que me importa\n");
  printf("\n");
  //printList(head);
  printf("\nElemento a ser eliminado: ");
  obten(head);
  printf(", Elemento eliminado: ");
  obten(head);
  printf("\n");
  deleteNode(head);
  //printList(head);
  while(head != NULL)
    {
      printf("\n");
      //printList(head);
      printf("\n");
      printf("Elemento a ser eliminado: ");
      obten(head);
      printf(", Elemento eliminado: ");
      obten(head);
      deleteNode(head);
      printf("\n");
      //printList(head);
      printf("\n");
      
      
    }
  return 0;
}

