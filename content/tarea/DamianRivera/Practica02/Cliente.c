#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
   
int main(int argc, char const *argv[]) 
{ 
    int sock = 0; 
    struct sockaddr_in serv_addr; 
    char buffer[1024] = {0}; 
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 
   
    memset(&serv_addr, '0', sizeof(serv_addr)); 
   
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(atoi(argv[2])); 
       
    //Configuracion con IPv4
    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)  
    { 
        printf("Error de IP \n"); 
        return -1; 
    } 
    //Conectamos al socket del servidor.
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        printf("Conexión Fallida \n"); 
        return -1; 
    } 

    char msg[256];
    char salida[] = "EOF";
    printf(" - Escribe 'EOF' para terminar. -\n");
    while(1){
        memset(msg, 0, 256);
        printf("Ingresa mensaje a enviar: \n");
        scanf("%s", msg);
        //Si se ingresa EOF terminamos el programa.
        if (*msg == *salida) {
            break;
        }
        send(sock , msg , strlen(msg) , 0 ); //enviamos mensaje al servidor
        recv( sock , buffer, 1024, 0); //recivimos mensaje del servidor
        printf("%s\n",buffer ); //imprimimos el mensaje recibido
        memset(msg, 0, strlen(msg));//limpiamos el mensaje enviado 
        memset(buffer, 0, strlen(buffer));//limpiamos el mensaje recibido
    }
    return 0; 
} 