#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 

int main(int argc, char const *argv[]) 
{ 
    int server_fd, new_socket;//enteros para los sockets 
    struct sockaddr_in address; 
    int opt = 1; 
    int addrlen = sizeof(address); 
    char *buffer = malloc(1024* sizeof(char));
       
    //Creamos el descriptor de archivo
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        perror("Fallo al crear el socket"); 
        exit(EXIT_FAILURE); 
    } 
       
    //COnfiguramos el socket 
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
    { 
        perror("Error en configuración del socket"); 
        exit(EXIT_FAILURE); 
    } 

    address.sin_family = AF_INET;//IPv4 
    address.sin_addr.s_addr = INADDR_ANY;//Escuchamos por cualquier dirección 
    address.sin_port = htons(atoi(argv[1])); //confirguramos el puerto por el que escucha
       
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) 
    { 
        perror("Error al crear el bind"); 
        exit(EXIT_FAILURE); 
    } 
    if (listen(server_fd, 3) < 0) 
    { 
        perror("Error al escuchar"); 
        exit(EXIT_FAILURE); 
    } 
    printf("- Servidor Escuchando -\n");
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) 
    { 
        perror("Error al conectar con el cliente"); 
        exit(EXIT_FAILURE); 
    } 

    printf("Conexión aceptada desde %d: %d\n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));
    while(1){
        recv( new_socket , buffer, 1024, 0); //recibimos del cliente
        char ejecutar[128];
        strcpy(ejecutar, "/bin/");
        strcat(ejecutar, buffer);

        char respuesta[1024] ;//buffer donde se guardará la salida.
        int out_pipe[2]//arreglo de entero para el pipe
        int saved_stdout;//enetero para salvar la salida
        saved_stdout = dup(STDOUT_FILENO);  //save stdout for display later 
        if( pipe(out_pipe) != 0) {
            exit(1);
        }

        dup2(out_pipe[1], STDOUT_FILENO);   //Redireccionamos la salida estandar
        close(out_pipe[1]);
        system(ejecutar);//ejecutamos el comando recibido del cliente
        fflush(stdout);
        read(out_pipe[0], respuesta, 1024); //leemos la salida del pipe y lo cargamos en un buffer
        dup2(saved_stdout, STDOUT_FILENO);
        send(new_socket , respuesta, strlen(respuesta) , 0 ); //enviamor la salida del comando
        
        printf("Respuesta Envíada\n"); 

        memset(respuesta, 0, strlen(respuesta));//vaciamos la respuesta enviada
        memset(ejecutar, 0, strlen(ejecutar));//vaciamos el comando a ejecutar
        memset(buffer, 0, strlen(buffer));//vaciamos el buffer donde guardamos el comando
    }
    
    return 0; 
} 